package ru.karpunin.tabletransform;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.io.File;
import java.io.IOException;
import java.net.URL;

@Testcontainers
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ContextConfiguration(initializers = AbstractIntegrationTest.DataSourceInitializer.class)
@ActiveProfiles("test")
public abstract class AbstractIntegrationTest {

    @Autowired
    protected WebTestClient webTestClient;

    @Autowired
    protected ObjectMapper objectMapper;

    @Container
    private static final PostgreSQLContainer<?> database =
            new PostgreSQLContainer<>(DockerImageName.parse("postgres:latest"))
                    .withDatabaseName("test_database")
                    .withUsername("test_container")
                    .withPassword("test_container");

    public static class DataSourceInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                    applicationContext,
                    "spring.datasource.url=" + database.getJdbcUrl(),
                    "spring.datasource.username=" + database.getUsername(),
                    "spring.datasource.password=" + database.getPassword()
            );
        }
    }

    protected <T> T getDtoFromJson(String path, Class<T> clazz) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(path);
        if (resource == null) {
            throw new IllegalArgumentException("File not found!");
        }
        return objectMapper.readValue(new File(resource.getPath()), clazz);
    }

    protected <T> T sendPutRequest(String path, Object bodyValue, Class<T> clazz) {
        return webTestClient.put()
                .uri(path)
                .bodyValue(bodyValue)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected <T> T sendPutRequestWithId(String path, int id, Object bodyValue, Class<T> clazz) {
        return webTestClient.put()
                .uri(path, id)
                .bodyValue(bodyValue)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected <T> T sendPatchRequest(String path, int id, Object bodyValue, Class<T> clazz) {
        return webTestClient.patch()
                .uri(path, id)
                .bodyValue(bodyValue)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected <T> T sendDeleteRequest(String path, String pathVariable, Class<T> clazz) {
        return webTestClient.delete()
                .uri(path + "/" + pathVariable)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected <T> T sendGetRequest(String path, int id, Class<T> clazz) {
        return webTestClient.get()
                .uri(path, id)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected <T> T sendGetRequestParam(String path, int id, String nameRequestParam, Object requestParam, Class<T> clazz) {
        return webTestClient.get()
                .uri(uriBuilder -> uriBuilder
                        .path(path)
                        .queryParam(nameRequestParam, requestParam)
                        .build(id))
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }

    protected void sendPostRequest(String path, int id, Object bodyValue) {
        webTestClient.post()
                .uri(uriBuilder -> uriBuilder
                        .path(path)
                        .build(id))
                .bodyValue(bodyValue)
                .exchange()
                .expectStatus()
                .isOk();
    }

    protected <T> T sendPostRequest(String path, Object bodyValue, Class<T> clazz) {
        return webTestClient.post()
                .uri(path)
                .bodyValue(bodyValue)
                .exchange()
                .expectBody(clazz)
                .returnResult()
                .getResponseBody();
    }
}
