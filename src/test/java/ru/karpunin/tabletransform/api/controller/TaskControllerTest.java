package ru.karpunin.tabletransform.api.controller;

import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.reflect.ConstructorMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import ru.karpunin.tabletransform.AbstractIntegrationTest;
import ru.karpunin.tabletransform.api.ApiPaths;
import ru.karpunin.tabletransform.api.dto.CreateTaskRequest;
import ru.karpunin.tabletransform.api.dto.Page;
import ru.karpunin.tabletransform.api.dto.TaskResponse;
import ru.karpunin.tabletransform.api.dto.UpdateTaskRequest;
import ru.karpunin.tabletransform.core.entity.Task;
import ru.karpunin.tabletransform.core.mapper.TaskMapper;
import ru.karpunin.tabletransform.core.repository.TaskRepository;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class TaskControllerTest extends AbstractIntegrationTest {
    @Autowired
    private TaskMapper taskMapper;

    @Autowired
    private Jdbi jdbi;

    @Autowired
    private TaskRepository taskRepository;

    @ParameterizedTest
    @MethodSource
    @Sql("/scripts/task/create_task.sql")
    void testCreateTask(String dtoPath, String[] comparingFields) throws IOException {

        CreateTaskRequest createTaskRequest = getDtoFromJson(dtoPath, CreateTaskRequest.class);

        TaskResponse taskResponse = sendPostRequest(ApiPaths.TASK, createTaskRequest, TaskResponse.class);

        assertThat(taskResponse)
                .usingRecursiveComparison()
                .comparingOnlyFields(comparingFields)
                .isEqualTo(taskMapper.map(taskMapper.map(createTaskRequest)));
    }

    public static Stream<Arguments> testCreateTask() {
        return Stream.of(
                Arguments.of("test_dtos/task/create_task/create_task.json",
                        new String[]{"targetSchema", "targetTable", "sourceSchema", "sourceTable"})
        );

    }

    @ParameterizedTest
    @MethodSource
    @Sql("/scripts/task/update_task.sql")
    void testUpdateTask(int id, String dtoPath, Task expectedTask) throws IOException {

        UpdateTaskRequest updateTaskRequest = getDtoFromJson(dtoPath, UpdateTaskRequest.class);

        sendPatchRequest(ApiPaths.TASK_BY_ID, id, updateTaskRequest, Void.class);

        try (Handle handle = jdbi.open()) {

            List<Task> resTask =
                    handle.registerRowMapper(Task.class, ConstructorMapper.of(Task.class))
                            .createQuery(
                                    "SELECT * FROM task WHERE id = " + id
                            )
                            .mapTo(Task.class)
                            .list();

            Assertions.assertEquals(
                    1,
                    resTask.size()
            );

            Task res = resTask.getFirst();

            Assertions.assertEquals(expectedTask, res);
        }
    }

    private static Stream<Arguments> testUpdateTask() {
        return Stream.of(
                Arguments.of(
                        1,
                        "test_dtos/task/update_task/update_task.json",
                        new Task(
                                1,
                                "new_source_schema",
                                "new_target_schema",
                                "new_source_table",
                                "new_target_table"
                        )
                )
        );
    }


    @ParameterizedTest
    @MethodSource
    @Sql("/scripts/task/delete_task.sql")
    void testDeleteTask (String dtoPath) throws IOException {
        CreateTaskRequest createTaskRequest = getDtoFromJson(dtoPath, CreateTaskRequest.class);
        sendPutRequest(ApiPaths.TASK, createTaskRequest, Void.class);
        sendDeleteRequest(ApiPaths.TASK, String.valueOf(1), Void.class);
        try (Handle handle = jdbi.open()) {
            List<Task> resNews =
                    handle.registerRowMapper(Task.class, ConstructorMapper.of(Task.class))
                            .createQuery("SELECT * FROM task WHERE id = 1")
                            .mapTo(Task.class)
                            .list();
            assertThat(resNews.size()).isEqualTo(0);
        }
    }

    private static Stream<Arguments> testDeleteTask() {
        return Stream.of(
                Arguments.of("test_dtos/task/create_task/create_task.json")
        );
    }

    @ParameterizedTest
    @MethodSource
    @Sql({"/scripts/task/get_task.sql"})
    void testGetTask(Integer pageSize, Integer page, String expectedDtoPath) throws IOException {
        Page<TaskResponse> expectedDto = getDtoFromJson(expectedDtoPath, Page.class);
        Page<TaskResponse> actualDto = getTaskRequest(pageSize, page, Page.class);
        Assertions.assertNotNull(actualDto);
        Assertions.assertEquals(expectedDto, actualDto);
    }

    private static Stream<Arguments> testGetTask() {
        return Stream.of(
                Arguments.of(2, 2, "test_dtos/task/get_task/task_response_with_limit_2_and_page_2.json"),
                Arguments.of(10, 44, "test_dtos/task/get_task/task_response_with_limit_10_and_page_2.json")
        );
    }






















    private <T> T getTaskRequest(Integer pageSize, Integer page, Class<T> expectedResponseClass) {
        return webTestClient.get()
                .uri(uri -> uri
                        .path(ApiPaths.TASK)
                        .queryParam("pageSize", pageSize)
                        .queryParam("page", page)
                        .build())
                .exchange()
                .expectBody(expectedResponseClass)
                .returnResult()
                .getResponseBody();
    }



}
