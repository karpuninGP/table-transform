TRUNCATE TABLE task CASCADE;

INSERT INTO task (id, source_schema, source_table, target_schema, target_table)
VALUES
    (1, 'source_schema1', 'source_table1', 'target_schema1', 'target_table1'),
    (2, 'source_schema2', 'source_table2', 'target_schema2', 'target_table2'),
    (3, 'source_schema3', 'source_table3', 'target_schema3', 'target_table3'),
    (4, 'source_schema4', 'source_table4', 'target_schema4', 'target_table4'),
    (5, 'source_schema5', 'source_table5', 'target_schema5', 'target_table5'),
    (6, 'source_schema6', 'source_table6', 'target_schema6', 'target_table6'),
    (7, 'source_schema7', 'source_table7', 'target_schema7', 'target_table7'),
    (8, 'source_schema8', 'source_table8', 'target_schema8', 'target_table8'),
    (9, 'source_schema9', 'source_table9', 'target_schema9', 'target_table9'),
    (10, 'source_schema10', 'source_table10', 'target_schema10', 'target_table10'),
    (11, 'source_schema11', 'source_table11', 'target_schema11', 'target_table11'),
    (12, 'source_schema12', 'source_table12', 'target_schema12', 'target_table12'),
    (13, 'source_schema13', 'source_table13', 'target_schema13', 'target_table13'),
    (14, 'source_schema14', 'source_table14', 'target_schema14', 'target_table14'),
    (15, 'source_schema15', 'source_table15', 'target_schema15', 'target_table15'),
    (16, 'source_schema16', 'source_table16', 'target_schema16', 'target_table16');