INSERT INTO python_type (name)
VALUES ('python_type1'),
       ('python_type2'),
       ('python_type3'),
       ('python_type4'),
       ('python_type5');

-- Insert into database_attribute_type table
INSERT INTO database_attribute_type (name, python_type_id)
VALUES ('attribute_type1', 1),
       ('attribute_type2', 2),
       ('attribute_type3', 3),
       ('attribute_type4', 4),
       ('attribute_type5', 5);