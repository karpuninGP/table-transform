CREATE INDEX task_target_table_index on task using hash(target_table);
CREATE INDEX task_source_table_index on task using hash(source_table);

CREATE INDEX column_name_index ON column_nm (name)
    WHERE name LIKE '%some_important_name';