create or replace procedure clear(
    toDate timestamptz,
    fromDate timestamptz
)
    language plpgsql
as
$$
begin

    DELETE
    FROM task_audit
    WHERE changed_on BETWEEN fromDate AND toDate;

    commit;
end;
$$;