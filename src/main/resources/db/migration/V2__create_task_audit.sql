CREATE TABLE task_audit
(
    id          serial primary key,
    task_id INT         NOT NULL,
    source_schema varchar(255) NOT NULL,
    source_table  varchar(255) NOT NULL,
    target_schema varchar(255) NOT NULL,
    target_table  varchar(255) NOT NULL,
    changed_on  timestamptz   NOT NULL
);

CREATE OR REPLACE FUNCTION log_last_name_changes()
    RETURNS TRIGGER
    LANGUAGE PLPGSQL
AS
$$
BEGIN
    IF NEW.target_table <> OLD.target_table
           AND NEW.source_table <> OLD.source_table
           AND NEW.target_schema <> OLD.target_schema
           AND NEW.source_schema <> OLD.source_schema
        THEN
        INSERT INTO task_audit(task_id, source_schema, source_table, target_schema, target_table, changed_on)
        VALUES(OLD.id, OLD.source_schema, OLD.source_table, OLD.target_schema, OLD.target_table, now());
    END IF;

    RETURN NEW;
END;
$$;

CREATE TRIGGER last_name_changes
    BEFORE UPDATE
    ON task
    FOR EACH ROW
EXECUTE PROCEDURE log_last_name_changes();