package ru.karpunin.tabletransform.core.config;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.postgres.PostgresPlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.karpunin.tabletransform.core.repository.TaskAuditRepository;
import ru.karpunin.tabletransform.core.repository.TaskRepository;
import ru.karpunin.tabletransform.core.repository.TransformationFunctionRepository;

import javax.sql.DataSource;

@Configuration
public class DatabaseConfig {

    @Bean
    public Jdbi jdbi(DataSource dataSource) {
        return Jdbi.create(dataSource)
                .installPlugin(new SqlObjectPlugin())
                .installPlugin(new PostgresPlugin());
    }

    @Bean
    public TaskRepository taskRepository(Jdbi jdbi) {
        return jdbi.onDemand(TaskRepository.class);
    }

    @Bean
    public TaskAuditRepository taskAuditRepository(Jdbi jdbi) {
        return jdbi.onDemand(TaskAuditRepository.class);
    }

    @Bean
    public TransformationFunctionRepository transformationFunctionRepository(Jdbi jdbi) {
        return jdbi.onDemand(TransformationFunctionRepository.class);
    }
}
