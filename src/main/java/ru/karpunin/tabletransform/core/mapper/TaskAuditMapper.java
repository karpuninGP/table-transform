package ru.karpunin.tabletransform.core.mapper;

import org.mapstruct.Mapper;
import ru.karpunin.tabletransform.api.dto.TaskAuditResponse;
import ru.karpunin.tabletransform.core.entity.TaskAudit;

@Mapper
public interface TaskAuditMapper {

    TaskAuditResponse map(TaskAudit dto);

}
