package ru.karpunin.tabletransform.core.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.karpunin.tabletransform.api.dto.CreateTaskRequest;
import ru.karpunin.tabletransform.api.dto.TaskResponse;
import ru.karpunin.tabletransform.api.dto.UpdateTaskRequest;
import ru.karpunin.tabletransform.core.entity.Task;

@Mapper
public interface TaskMapper {
    @Mapping(target = "id", ignore = true)
    Task map(CreateTaskRequest dto);

    TaskResponse map(Task dto);

    @Mapping(target = "id", ignore = true)
    Task map(UpdateTaskRequest dto);
}
