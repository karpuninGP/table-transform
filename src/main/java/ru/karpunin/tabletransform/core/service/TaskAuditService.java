package ru.karpunin.tabletransform.core.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.karpunin.tabletransform.api.dto.Page;
import ru.karpunin.tabletransform.api.dto.TaskAuditFilter;
import ru.karpunin.tabletransform.api.dto.TaskAuditResponse;
import ru.karpunin.tabletransform.core.mapper.TaskAuditMapper;
import ru.karpunin.tabletransform.core.repository.TaskAuditRepository;

import java.time.OffsetDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskAuditService {

    private final TaskAuditRepository taskAuditRepository;

    private final TaskAuditMapper taskAuditMapper;

    public Page<TaskAuditResponse> getTaskAudits(TaskAuditFilter taskAuditFilter) {
        OffsetDateTime to = taskAuditFilter.getTo();
        OffsetDateTime from = taskAuditFilter.getFrom();
        Integer page = taskAuditFilter.getPage();
        Integer pageSize = taskAuditFilter.getPageSize();

        if (from.isAfter(to)) {
            throw new RuntimeException("Incorrect time interval!");
        }

        Integer totalPagesAmount = taskAuditRepository.getTotalPagesAmount(pageSize);

        page = Math.min(page, totalPagesAmount);

        Integer offset = Math.max(page - 1, 0) * pageSize;

        List<TaskAuditResponse> list = taskAuditRepository.getTaskAudits(to, from, pageSize, offset).stream().map(taskAuditMapper::map).toList();

        return new Page<>(list, page, totalPagesAmount);
    }

    public void deleteAudit(OffsetDateTime to, OffsetDateTime from) {
        taskAuditRepository.deleteAudit(to, from);
    }
}
