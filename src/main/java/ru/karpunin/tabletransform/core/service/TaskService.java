package ru.karpunin.tabletransform.core.service;

import lombok.RequiredArgsConstructor;
import org.jdbi.v3.core.Handle;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.reflect.ConstructorMapper;
import org.springframework.stereotype.Service;
import ru.karpunin.tabletransform.api.dto.*;
import ru.karpunin.tabletransform.core.entity.Task;
import ru.karpunin.tabletransform.core.mapper.TaskMapper;
import ru.karpunin.tabletransform.core.repository.TaskRepository;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@RequiredArgsConstructor
public class TaskService {

    private final Jdbi jdbi;

    private final TaskRepository taskRepository;

    private final TaskMapper taskMapper;

    public TaskResponse saveTask(CreateTaskRequest createTaskRequest) {
        return taskMapper.map(taskRepository.saveTask(taskMapper.map(createTaskRequest)));
    }

    public TaskResponse getTaskById(Integer id) {

        Task task = taskRepository.getTaskById(id).orElseThrow();

        return taskMapper.map(task);
    }

    public Page<TaskResponse> getTasks(TaskFilter taskFilter) {
        Integer totalPagesAmount = taskRepository.getTotalPagesAmount(taskFilter.getPageSize());

        int page = Math.min(taskFilter.getPage(), totalPagesAmount);

        Integer offset = (Math.max(page - 1, 0)) * taskFilter.getPageSize();

        List<TaskResponse> list = (taskRepository.getTasks(taskFilter.getPageSize(), offset)).stream().map(taskMapper::map).toList();

        return new Page<>(list, page, totalPagesAmount);
    }

    public void deleteTaskById(Integer id) {
        taskRepository.deleteTaskById(id);
    }

    public TaskResponse updateTaskById(Integer id, UpdateTaskRequest updateTaskRequest) {
        if (!taskRepository.existsById(id)) {
            throw new NoSuchElementException();
        }

        return taskMapper.map(taskRepository.updateTaskById(id, taskMapper.map(updateTaskRequest)));
    }

    public List<TaskResponse> getTasksWithFilters(String sortBy, String sortDirection) {
        try (Handle handle = jdbi.open()) {
            return handle.registerRowMapper(Task.class, ConstructorMapper.of(Task.class))
                    .createQuery("SELECT * FROM task ORDER BY " + sortBy + " " + sortDirection)
                    .mapTo(Task.class)
                    .list().stream().map(taskMapper::map).toList();
        }
    }

    public List<TaskWindowedResponse> getColumnCountPerTask() {
        return taskRepository.getColumnCount();
    }
}
