package ru.karpunin.tabletransform.core.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.karpunin.tabletransform.api.dto.CreateColumnTransformationFunctionRequest;
import ru.karpunin.tabletransform.core.repository.TransformationFunctionRepository;

@Service
@RequiredArgsConstructor
public class TransformationFunctionService {

    private final TransformationFunctionRepository transformationFunctionRepository;

    public void createTransformationFunctionAndColumn(
            int id,
            CreateColumnTransformationFunctionRequest request
    ) {
        transformationFunctionRepository.createTransformationFunctionAndColumn(
                id,
                request
        );
    }
}
