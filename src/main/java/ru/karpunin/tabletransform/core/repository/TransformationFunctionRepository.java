package ru.karpunin.tabletransform.core.repository;

import org.jdbi.v3.sqlobject.config.RegisterConstructorMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindMethods;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;
import ru.karpunin.tabletransform.api.dto.CreateColumnTransformationFunctionRequest;
import ru.karpunin.tabletransform.core.entity.Column;
import ru.karpunin.tabletransform.core.entity.TransformationFunction;

@RegisterConstructorMapper(TransformationFunction.class)
@RegisterConstructorMapper(Column.class)
public interface TransformationFunctionRepository {
    @Transaction
    @SqlUpdate("""
            
            BEGIN TRANSACTION;
            
            INSERT INTO transformation_function (id, name, body, python_type_id) VALUES 
            (:id, :request.tfName, :request.body, :request.pythonTypeId);
            
            INSERT INTO column_nm (id, name, database_attribute_type_id, task_id) VALUES 
            (:id, :request.columnName, :request.dbAttributeTypeId, :request.taskId);
            
            COMMIT;
            """)
    void createTransformationFunctionAndColumn(
            @Bind("id") int id,
            @BindMethods("request") CreateColumnTransformationFunctionRequest columnRequest
            );
}
