package ru.karpunin.tabletransform.core.repository;

import org.jdbi.v3.sqlobject.config.RegisterConstructorMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindMethods;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import ru.karpunin.tabletransform.api.dto.TaskWindowedResponse;
import ru.karpunin.tabletransform.core.entity.Task;

import java.util.List;
import java.util.Optional;

@RegisterConstructorMapper(value = Task.class)
@RegisterConstructorMapper(value = TaskWindowedResponse.class)
public interface TaskRepository {

    @SqlUpdate("""
                INSERT INTO task(source_schema, source_table, target_schema, target_table)
                VALUES (
                :task.sourceSchema,
                :task.sourceTable,
                :task.targetSchema,
                :task.targetTable
                )
            """)
    @GetGeneratedKeys
    Task saveTask(@BindMethods(value = "task") Task task);

    @SqlQuery("""
            SELECT * FROM task WHERE id = :id
            """)
    Optional<Task> getTaskById(@Bind("id") Integer id);

    @SqlQuery("""
                        SELECT CEIL(COUNT(*)::FLOAT / :pageSize) FROM task
            """)
    Integer getTotalPagesAmount(@Bind("pageSize") Integer pageSize);


    @SqlQuery("""
            SELECT * FROM task
            ORDER BY task.id
            LIMIT :limit
            OFFSET :offset
            """)
    List<Task> getTasks(@Bind("limit") Integer limit, @Bind("offset") Integer offset);

    @SqlUpdate("""
            DELETE FROM task WHERE id = :id
            """)
    void deleteTaskById(@Bind("id") Integer id);

    @SqlQuery("""
            SELECT EXISTS(SELECT * FROM task WHERE id = :id)
            """)
    boolean existsById(@Bind("id") Integer id);

    @SqlUpdate("""
            UPDATE task
            SET target_schema            = :task.targetSchema::VARCHAR,
                target_table             = :task.targetTable::VARCHAR,
                source_schema            = :task.sourceSchema::VARCHAR,
                source_table             = :task.sourceTable::VARCHAR
            WHERE task.id = :id
            """)
    @GetGeneratedKeys
    Task updateTaskById(@Bind("id") Integer id, @BindMethods("task") Task task);

    @SqlQuery("""
            SELECT
                t.id AS id,
                t.source_schema,
                t.source_table,
                t.target_schema,
                t.target_table,
                COUNT(c.name) OVER (PARTITION BY t.id) AS column_count
            FROM
                task t
            LEFT JOIN
                column_nm c ON t.id = c.task_id
                """)
    List<TaskWindowedResponse> getColumnCount();
}
