package ru.karpunin.tabletransform.core.repository;

import org.jdbi.v3.sqlobject.config.RegisterConstructorMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import ru.karpunin.tabletransform.core.entity.TaskAudit;

import java.time.OffsetDateTime;
import java.util.List;

@RegisterConstructorMapper(value = TaskAudit.class)
public interface TaskAuditRepository {
    @SqlQuery("""
            SELECT * FROM task_audit
            WHERE changed_on
            BETWEEN :after AND :before
            OFFSET :offset
            LIMIT :limit
            """)
    List<TaskAudit> getTaskAudits(@Bind("before")OffsetDateTime before, @Bind("after") OffsetDateTime after, @Bind("limit") Integer limit, @Bind("offset") Integer offset);

    @SqlQuery("""
                        SELECT CEIL(COUNT(*)::FLOAT / :pageSize) FROM task_audit
            """)
    Integer getTotalPagesAmount(@Bind("pageSize") Integer pageSize);

    @SqlUpdate("""
            CALL clear(:to, :from)
            """)
    void deleteAudit(@Bind("to") OffsetDateTime to, @Bind("from") OffsetDateTime from);
}
