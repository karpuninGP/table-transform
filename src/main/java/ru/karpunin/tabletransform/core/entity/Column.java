package ru.karpunin.tabletransform.core.entity;

public record Column(
        int id,
        String name,
        int dbAttributeTypeId,
        int taskId
) {
}
