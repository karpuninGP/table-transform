package ru.karpunin.tabletransform.core.entity;

public record Task(
        Integer id,

        String sourceSchema,

        String targetSchema,

        String sourceTable,

        String targetTable
) {
}
