package ru.karpunin.tabletransform.core.entity;

import java.time.OffsetDateTime;

public record TaskAudit(
        String sourceSchema,

        String targetSchema,

        String sourceTable,

        String targetTable,

        OffsetDateTime changedOn
) {
}
