package ru.karpunin.tabletransform.core.entity;

public record TransformationFunction(
        int id,
        String name,
        String body,
        int pythonTypeId
) {
}
