package ru.karpunin.tabletransform.api;

public class ApiPaths {

    public static final String TASK = "/api/v1/task";

    public static final String TASK_BY_ID = "/api/v1/task/{id}";

    public static final String TASK_AUDIT = "/api/v1/task/audit";

    public static final String TASK_COLUMN_COUNT = "/api/v1/task/columns";

    public static final String TASK_FILTER = "/api/v1/task/filter";

    public static final String TRANSFORMATION_FUNCTION = "/api/v1/task/tf/{id}";
}
