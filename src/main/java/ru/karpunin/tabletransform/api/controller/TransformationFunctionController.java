package ru.karpunin.tabletransform.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.karpunin.tabletransform.api.ApiPaths;
import ru.karpunin.tabletransform.api.dto.CreateColumnTransformationFunctionRequest;
import ru.karpunin.tabletransform.core.service.TransformationFunctionService;

@Controller
@Tag(name = "Transformation Function API")
@RequiredArgsConstructor
@Validated
public class TransformationFunctionController {
    private final TransformationFunctionService tfService;

    @Operation(description = "Создание новой функции трансформации и колонки")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Функция и колонка созданы"
            )
    })
    @PostMapping(ApiPaths.TRANSFORMATION_FUNCTION)
    @ResponseBody
    public void postTransformationFunctionAndColumn(
            @PathVariable int id,
            @RequestBody CreateColumnTransformationFunctionRequest request
    ) {
        tfService.createTransformationFunctionAndColumn(id, request);
    }
}
