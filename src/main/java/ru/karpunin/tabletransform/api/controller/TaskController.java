package ru.karpunin.tabletransform.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import ru.karpunin.tabletransform.api.ApiPaths;
import ru.karpunin.tabletransform.api.dto.*;
import ru.karpunin.tabletransform.core.service.TaskService;

@Controller
@Tag(name = "Task API")
@RequiredArgsConstructor
@Validated
public class TaskController {

    private final TaskService taskService;

    @Operation(description = "Создание новой задачи")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Новая задача создана"
            )
    })
    @PostMapping(ApiPaths.TASK)
    public ResponseEntity<TaskResponse> saveTask(@Valid @RequestBody CreateTaskRequest createTaskRequest) {
        return ResponseEntity.ok(taskService.saveTask(createTaskRequest));
    }

    @Operation(description = "Получение задачи по идентификатору")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Задача получена"
            )
    })
    @GetMapping(ApiPaths.TASK_BY_ID)
    public ResponseEntity<TaskResponse> getTaskById(@PathVariable Integer id) {
        return ResponseEntity.ok(taskService.getTaskById(id));
    }

    @Operation(description = "Получение страницы из задач")
    @GetMapping(ApiPaths.TASK)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Список задач получен"
            )
    })
    public ResponseEntity<Page<TaskResponse>> getTasks(TaskFilter taskFilter) {
        return ResponseEntity.ok(taskService.getTasks(taskFilter));
    }

    @Operation(description = "Удаление задачи по идентификатору")
    @DeleteMapping(ApiPaths.TASK_BY_ID)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Задача удалена"
            )
    })
    @ResponseBody
    public void deleteTask(@PathVariable Integer id) {
        taskService.deleteTaskById(id);
    }

    @Operation(description = "Обновление задачи по идентификатору")
    @PatchMapping(ApiPaths.TASK_BY_ID)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Задача обновлена"
            )
    })
    @ResponseBody
    public TaskResponse updateTask(
            @PathVariable Integer id,
            @Valid @RequestBody UpdateTaskRequest updateTaskRequest
    ) {
        return taskService.updateTaskById(id, updateTaskRequest);
    }

    @Operation(description = "Получение задач по фильтру")
    @GetMapping(ApiPaths.TASK_FILTER)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Список задач в соответствии с фильтром"
            )
    })
    public ResponseEntity<List<TaskResponse>> getTasks(
            @RequestParam(defaultValue = "source_table") String sortBy,
            @RequestParam(defaultValue = "ASC") String sortDirection
    ) {
        return ResponseEntity.ok(taskService.getTasksWithFilters(sortBy, sortDirection));
    }

    @Operation(description = "Получение количества преобразованных колонок в рамках одной задачи")
    @GetMapping(ApiPaths.TASK_COLUMN_COUNT)
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Список задач в соответствии с фильтром"
            )
    })
    public ResponseEntity<List<TaskWindowedResponse>> getTasks() {
        return ResponseEntity.ok(taskService.getColumnCountPerTask());
    }

}
