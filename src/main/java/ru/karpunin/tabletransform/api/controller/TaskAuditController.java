package ru.karpunin.tabletransform.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.karpunin.tabletransform.api.ApiPaths;
import ru.karpunin.tabletransform.api.dto.Page;
import ru.karpunin.tabletransform.api.dto.TaskAuditFilter;
import ru.karpunin.tabletransform.api.dto.TaskAuditResponse;
import ru.karpunin.tabletransform.core.service.TaskAuditService;

import java.time.OffsetDateTime;

@Controller
@Tag(name = "Task API")
@RequiredArgsConstructor
@Validated
public class TaskAuditController {

    private final TaskAuditService taskAuditService;

    @Operation(description = "Получение аудита по задачам по временному интервалу")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Аудит получен"
            )
    })
    @GetMapping(ApiPaths.TASK_AUDIT)
    public ResponseEntity<Page<TaskAuditResponse>> getTaskById(TaskAuditFilter taskAuditFilter) {
        return ResponseEntity.ok(taskAuditService.getTaskAudits(taskAuditFilter));
    }

    @Operation(description = "Удаление аудитов во временном интервале")
    @ApiResponses(value = {
            @ApiResponse(
                    responseCode = "200",
                    description = "Аудит удален"
            )
    })
    @ResponseBody
    @DeleteMapping(ApiPaths.TASK_AUDIT)
    public void getTaskById(@RequestParam OffsetDateTime to, @RequestParam OffsetDateTime from) {
        taskAuditService.deleteAudit(to, from);
    }
}
