package ru.karpunin.tabletransform.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Getter
@Setter
public class TaskAuditFilter extends PageFilter{
    private OffsetDateTime to;

    private OffsetDateTime from;
}
