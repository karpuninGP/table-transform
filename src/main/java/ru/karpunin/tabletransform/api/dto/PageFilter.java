package ru.karpunin.tabletransform.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class PageFilter {

    protected Integer pageSize;
    protected Integer page;

    PageFilter() {
        pageSize = 10;
        page = 1;
    }

}
