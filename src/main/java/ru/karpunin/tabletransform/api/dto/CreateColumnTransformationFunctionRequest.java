package ru.karpunin.tabletransform.api.dto;

public record CreateColumnTransformationFunctionRequest(
        String columnName,
        int dbAttributeTypeId,
        int taskId,

        String tfName,
        String body,
        int pythonTypeId

) {
}
