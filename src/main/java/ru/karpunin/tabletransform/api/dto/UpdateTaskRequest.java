package ru.karpunin.tabletransform.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

public record UpdateTaskRequest(
        @Schema(description = "Схема-источник для преобразования", example = "public")
        @Size(max = 100)
        String sourceSchema,

        @Schema(description = "Схема-цель для преобразования", example = "public")
        @Size(max = 100)
        String targetSchema,

        @Schema(description = "Таблица-источник для преобразования", example = "table_example")
        @Size(max = 100)
        @NotBlank
        String sourceTable,

        @Schema(description = "Таблица-цель для преобразования", example = "table_example")
        @Size(max = 100)
        @NotBlank
        String targetTable
) {

}
