package ru.karpunin.tabletransform.api.dto;

import java.util.List;

public record Page<T>(

        List<T> data,

        Integer page,

        Integer totalPagesAmount

) {}
