package ru.karpunin.tabletransform.api.dto;

import java.time.OffsetDateTime;

public record TaskAuditResponse(
        String sourceSchema,

        String targetSchema,

        String sourceTable,

        String targetTable,

        OffsetDateTime changedOn
) {
}
