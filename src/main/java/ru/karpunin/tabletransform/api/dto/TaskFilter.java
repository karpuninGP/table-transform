package ru.karpunin.tabletransform.api.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TaskFilter extends PageFilter {
}
