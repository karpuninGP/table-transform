package ru.karpunin.tabletransform.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public record TaskWindowedResponse(

        @Schema(description = "Идентификатор задачи")
        Integer id,

        @Schema(description = "Схема-источник для преобразования", example = "public")
        String sourceSchema,

        @Schema(description = "Схема-цель для преобразования", example = "public")
        String targetSchema,

        @Schema(description = "Таблица-источник для преобразования", example = "table_example")
        String sourceTable,

        @Schema(description = "Таблица-цель для преобразования", example = "table_example")
        String targetTable,

        @Schema(description = "Количество преобразованных колонок")
        Integer columnCount
) {
}
